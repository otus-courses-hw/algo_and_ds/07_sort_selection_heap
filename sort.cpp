#include "sort.hpp"
#include <cstddef>
#include <utility>
#include <vector>
#include <iterator>
#include <cassert>

namespace sort
{
    namespace {
        info algo_stat;

        auto gt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs > rhs;
        }

        unsigned max(unsigned pos, const std::vector<int> &vec)
        {
            auto max_i = 0U;
            for (auto i = 1U; i <= pos; ++i)
                if (gt(vec.at(i), vec.at(max_i)))
                    max_i = i;
            return max_i;
        }

        void heapify(iter_t begin, iter_t end, iter_t root)
        {
            auto dist = std::distance(begin, root); algo_stat.assignment++;
            auto L = std::next(begin, dist * 2 + 1); algo_stat.assignment++;
            auto R = std::next(begin, dist * 2 + 2); algo_stat.assignment++;
            auto X = root; algo_stat.assignment++;

            if (L < end && gt(*L, *X))
            {
                X = L;
                algo_stat.assignment++;
            }
            if (R < end && gt(*R, *X))
            {
                X = R;
                algo_stat.assignment++;
            }

            algo_stat.comparison++;
            if (X == root)
                return;

            std::swap(*X, *root); algo_stat.assignment += 3;
            heapify(begin, end, X);

        }

        void helper_heap(iter_t begin, iter_t end)
        {

            for(auto root = std::next(begin, algo_stat.elements / 2 - 1); root >= begin; std::advance(root, -1))
            {
                heapify(begin, end, root);
            }

            for(auto j = std::prev(end); j >= begin; std::advance(j, -1))
            {
                std::swap(*begin, *j); algo_stat.assignment += 3;
                heapify(begin, j, begin);
            }
        }

    }

    info heap(std::vector<int> &vec)
    {
        algo_stat.name = "heap";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto begin = vec.begin();
        auto end = vec.end();
        auto size = std::distance(begin, end);

        helper_heap(begin, end);

        return algo_stat;
    }

    info selection(std::vector<int> &vec)
    {
        algo_stat.name = "selection";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        for (auto j = vec.size() - 1; j > 0; --j)
        {
            std::swap(vec[max(j, vec)], vec[j]);
            algo_stat.assignment += 3;
        }

        return algo_stat;
    }
}
